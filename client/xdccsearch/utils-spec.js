import test from 'tape'
import deepFreeze from 'deep-freeze'
import {sizeFormatter} from './utils.js'

test('size formatting', (t) => {
  t.equal(
      sizeFormatter(1000),
      '1000 B',
      ''
  )

  t.equal(
      sizeFormatter(1024),
      '1 KB',
      ''
  )

  t.equal(
      sizeFormatter(2048),
      '2 KB',
      ''
  )

  t.equal(
      sizeFormatter(4194304),
      '4 MB',
      ''
  )

  t.equal(
      sizeFormatter(4194304000),
      '3.91 GB',
      ''
  )

  t.end()
})
