
<ResultListCounts>
  <div class="mdl-color--indigo-50 search">
    <h6 class="mdl-typography--title-color-contrast">
      <i class="fa fa-search"></i> result sets
    </h2>

    <ul>
      <li each={ query in queries }>
        <a href="#" onclick={ parent.selectResultSet } class={ selected: query==current }>{ query }</a>
        ({ countsFor[query] })
        <a href="#" onclick={ parent.clearResultSet }><i class="fa fa-remove"></i></a>
      </li>
    </ul>
  </div>

  <style scope>
    .search {
      padding: 1em;
    }

    h6 {
      margin-top: 0;
    }

    ul {
      padding-left: 0;
      margin: 0;
    }

    ul > li {
      display: inline;
      padding-right: 1em;
    }

    a.selected {
      text-decoration: underline;
    }
  </style>

  <script type="babel">
    this.mixin('redux')
    import { createSelector } from 'reselect'
    import { clearResults, getQueries, getCurrentQuery, selectQuery } from './results-duck'

    const selector = createSelector([getQueries, getCurrentQuery], (queryNodes, current) => {
      const queries = Object.keys(queryNodes).sort(
        (a, b) => {
          return a.toLowerCase().localeCompare(b.toLowerCase())
        }
      )
      const countsFor = {}

      queries.map(q => {
        if (queryNodes[q].packIds !== undefined)
          countsFor[q] = queryNodes[q].packIds.length
      })

      return {
        queries,
        current,
        countsFor
      }
    })

    this.subscribe( selector )

    this.selectResultSet = clickevent => {
      this.dispatch( selectQuery( clickevent.item.query ) )
    }

    this.clearResultSet = clickevent => {
      this.dispatch( clearResults( clickevent.item.query ) )
    }
  </script>
</ResultListCounts>
