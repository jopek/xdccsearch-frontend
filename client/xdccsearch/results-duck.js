import * as xhr from 'xhr-promise-redux'
import _ from 'lodash'
import { createSelector } from 'reselect'

const RESULTS_SELECT = 'results/SELECT'
const RESULTS_SELECT_QUERY = 'results/SELECT_QUERY'
const RESULTS_RECEIVE = 'results/RECEIVE'
const RESULTS_RECEIVE_PAGE_INFO = 'results/RECEIVE_PAGE_INFO'
const RESULTS_CLEAR = 'results/CLEAR'
const RESULTS_SORT_BY = 'results/SORT_BY'

export const initialState = {
  queries: {},
  current: '',
  packs: {},
  new: {},
  refs: {},
  sortBy: {field: '', direction: 'asc'}
}

export default function reducer(state = initialState, action = {}) {
  switch (action.type) {

    case RESULTS_RECEIVE: {
      return saveResultSet(state, action)
    }

    case RESULTS_CLEAR: {
      return clearResultSet(state, action.query)
    }

    case RESULTS_RECEIVE_PAGE_INFO: {
      return saveResultSetPageInfo(state, action)
    }

    case RESULTS_SELECT_QUERY: {
      return selectResultQuery(state, action.query)
    }

    case RESULTS_SORT_BY: {
      return sortResultsBy(state, action)
    }

    default: return state;
  }
}

const clearResultSet = (state, query) => {
  const newState = _.cloneDeep(state)

  if (newState.queries[query] !== undefined && newState.queries[query].packIds !== undefined)
    newState.queries[query].packIds.map(pid => {
      if (newState.refs[pid] > 1) {
        newState.refs[pid]--
      } else if (newState.refs[pid] == 1) {
        delete newState.packs[pid]
        delete newState.refs[pid]
      }
    })

  delete newState.queries[query]
  newState.new = {}

  if (newState.current == query)
    newState.current = ''

  return newState
}

const saveResultSet = (state, action) => {
  const query = action.query
  const resultPacks = _.mapKeys(action.results, 'pid')
  const packs = _.assignIn({}, resultPacks, state.packs)
  const refs = _.assignIn({}, state.refs)

  let packIds = action.results.map(p => p.pid)
  if (state.queries[query] !== undefined) {
    packIds = _.union(state.queries[query].packIds, packIds)
  }

  const newPackIds = {}
  action.results.map(p => {
    let packsRefs
    if (refs[p.pid] !== undefined)
      packsRefs = refs[p.pid] + 1
    else 
      packsRefs = refs[p.pid] || 1
    refs[p.pid] = packsRefs

    newPackIds[p.pid] = true
  })

  const queries = _.assign({}, state.queries)
  queries[query] = _.assign({}, state.queries[query], {packIds})

  return _.assign( {}, state, { packs, queries, current: query, new: newPackIds, refs } )
}

const saveResultSetPageInfo = (state_, action) => {
  const state = _.cloneDeep(state_)
  const hasMorePages =  action.pageNum + 1 < action.pageCount
  const queries = state.queries
  const query = action.query

  queries[query] = _.assignIn({}, queries[query], {
    currentPage: action.pageNum,
    hasMorePages
  })

  return _.assignIn({}, state, { queries })
}

const selectResultQuery = (state, query) => {
   return _.assignIn({}, state, {current: query})
}

const sortResultsBy = (state, action) => {
   return _.assignIn({}, state, {
     sortBy: {
       field: action.field,
       direction: action.direction
     }
   })
}


// ACTION Creators
export const selectQuery = query => {
  return {
    type: RESULTS_SELECT_QUERY,
    query
  }
}

export const selectPack = pid => {
  return {
    type: RESULTS_SELECT,
    id: pid
  }
}

export const receiveResults = (query, response) => {
  return {
    type: RESULTS_RECEIVE,
    query,
    results: response.results
  }
}

export const clearResults = (query) => {
  return {
    type: RESULTS_CLEAR,
    query
  }
}

export const sortBy = (field, direction = 'asc') => {
  return {
    type: RESULTS_SORT_BY,
    field,
    direction
  }
}

export const receivePageInfo = (query, response) => {
  return {
    type: RESULTS_RECEIVE_PAGE_INFO,
    query,
    pageNum: response.pn,
    pageCount: response.pc,
  }
}


// selectors
export const getCurrentQuery = state => {
  return state.results.current
}

export const getQueries = state => {
  return state.results.queries
}

export const getPacks = state => {
  return state.results.packs
}

export const getNewPackIds = state => {
  return state.results.new
}

export const getCurrentResults = createSelector(
  [getCurrentQuery, getQueries],
  (current, queries) => {
    return queries[current] || {}
  }
)

export const getPackIds = createSelector(
  [getCurrentResults], (results) => {
    return (results.packIds !== undefined) ? results.packIds : []
  }
)

const getSort = state => state.results.sortBy

export const getCurrentPacks = createSelector(
  [getPackIds, getPacks, getSort], (packIds, packs, sortBy) => {
    const sortByField = sortBy.field || ''

    let sortedPackIds

    if (sortByField.length > 0) {
      sortedPackIds = packIds.sort((ida, idb) => {
        const packa = packs[ida] || null 
        const packb = packs[idb] || null

        if (packa == null || packb == null)
          return 0

        const a = packa[sortByField]
        const b = packb[sortByField]

        if ( typeof a == 'string' )
          return a.toLowerCase().localeCompare(b.toLowerCase());
        if ( typeof a == 'number' )
          return a - b;
        return -1;
      })
    } else {
      sortedPackIds = packIds
    }
    return sortedPackIds.map(id => {return packs[id]})
  }
)
