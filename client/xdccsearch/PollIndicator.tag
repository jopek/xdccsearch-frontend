
<PollIndicator>
  <a onclick={togglePoll}>
    <span if={!isPolling}>start { }</span>
    <span if={isPolling}>stop { }</span>
    polling
  </a>
  <span class="tick {selected: requesting}">
    <i class="fa {fa-heart-o: !requesting} {fa-heart: requesting}"></i>
  </span>

  <style scoped>
    .selected.tick {
      opacity: 1;
      transition:none !important;
    }

    .tick {
      padding: 2px;
      opacity: 0.4;
      transition: 0.2s;
    }
  </style>

  <script type="babel">
    this.mixin('redux')

    import { createSelector } from 'reselect'
    import { pollXfers, stopPoll } from './transfers-duck'

    const getTransfersIsPolling = state => state.transfers.isPolling
    const getTransferIsRequesting = state => state.transfers.requesting

    const selector = createSelector(
      [getTransfersIsPolling, getTransferIsRequesting],
      (isPolling, requesting) => {
        return {
          isPolling,
          requesting
        }
      }
    )

    // start polling in the beginning
    this.dispatch(pollXfers())

    this.subscribe( selector )

    this.togglePoll = e => {
      this.isPolling ? this.dispatch(stopPoll()):
      this.dispatch(pollXfers())
    }
  </script>
</PollIndicator>
