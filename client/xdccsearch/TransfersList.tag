<TransfersList>
  <div if={ transferPids.length === 0 } class="empty-list">
    <h2 class="mdl-typography--display-2-color-contrast mdl-typography--text-center">no current downloads</h2>
  </div>

  <div class="cards-section" if={ transferPids.length > 0 } >
    <h2 class="mdl-typography--display-2-color-contrast"><i class="fa fa-download"></i> downloads</h2>

    <TransfersListItem
      each={pid in transferPids}
      transfer={parent.transfers[pid]}
      active={parent.active[pid]}
      handle_search_for={parent.searchForName}
      handle_stop_transfer={parent.stopTransferHandler}
      handle_remove_from_list={parent.removeFromListHandler}
      >
    </TransfersListItem>
  </div>

  <style type="text/css" media="screen" scoped>
    .mdl-card {
      min-height: auto;
      width: 100%;
    }

    div.empty-list {
      min-height: 60vh;
      display: flex;
    }

    div > h2 {
      text-align: center;
    }

    div.empty-list > h2 {
      margin: auto;
    }
  </style>

  <script type="babel">
    this.mixin('redux')

    import { createSelector } from 'reselect'
    import { stopTransfer, removeTransfer } from './transfers-duck'
    import { query } from './search-duck'

    const getTransfers = state => state.transfers.transfers
    const getActiveTransfers = state => state.transfers.active

    const selector = createSelector(
      [getTransfers, getActiveTransfers],
      (transfers, active) => {
        const transferPids = Object.keys(transfers).map(pid => parseInt(pid))
        return {
          transferPids,
          transfers,
          active
        }
      }
    )

    this.subscribe( selector )

    this.stopTransferHandler = mouseEvent => this.dispatch(stopTransfer(mouseEvent.item.pid))

    this.removeFromListHandler = mouseEvent => this.dispatch(removeTransfer(mouseEvent.item.pid))

    this.searchForName = e => {
      const pid = e.item.pid
      const pack = this.transfers[pid].pack
      this.dispatch(query(pack.name))
    }
  </script>
</TransfersList>
