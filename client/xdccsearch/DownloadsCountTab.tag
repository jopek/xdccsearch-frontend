<DownloadsCountTab>
  <span class={mdl-badge: downloadsCount > 0} data-badge={downloadsCount} >Downloads</span>

  <script>
    this.mixin('redux')
    import { createSelector } from 'reselect'

    const getActiveTransfers = state => state.transfers.active
    const downloadsCountSelector = createSelector(
      [getActiveTransfers],
      (active) => {
        return {
          downloadsCount: Object.keys(active).length
        }
      }
    )

    this.subscribe(downloadsCountSelector)
  </script>


</DownloadsCountTab>