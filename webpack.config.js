var webpack = require('webpack');
var HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
  entry: __dirname + '/client/index.js',

  devtool: "cheap-source-map",

  output: {
    path: __dirname + '/public',
    filename: 'bundle.js'
  },

  plugins: [
    new webpack.ProvidePlugin({
      riot: 'riot'
    }),
    new HtmlWebpackPlugin({
      template: './index.template.html'
    })
  ],

  module: {
    preLoaders: [
      { test: /\.tag$/,
        exclude: /node_modules/,
        loader: 'riotjs-loader',
      }
    ],
    loaders: [
      {
        test: /\.js$|\.tag$/,
        exclude: /node_modules/,
        loader: 'babel-loader',
        query: {
          presets: ['es2015']
        },
      }, {
        test: /\.css$/,
        loader: "style!css"
      }, {
        test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "url-loader?limit=10000&minetype=application/font-woff"
      }, {
        test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
        loader: "file-loader"
      }
    ]
  },

  devServer: {
    contentBase: './public'
  }
};
