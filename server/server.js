var webpack = require('webpack')
var webpackDevMiddleware = require('webpack-dev-middleware')
var webpackHotMiddleware = require('webpack-hot-middleware')
var config = require('../webpack.config')

var bodyparser = require('simple-bodyparser');
var request = require('request');

var express = require('express');
var app = new express();
var port = 8000


var compiler = webpack(config)
app.use(webpackDevMiddleware(compiler, { noInfo: true, publicPath: config.output.path }))
app.use(webpackHotMiddleware(compiler))
app.use(bodyparser());

app.get("/", function(req, res) {
  res.sendFile( config.output.path + '/index.html')
});

var proxy = false;
// var proxyRequest = request.defaults({baseUrl: 'http://ixirc.com/api'});
var proxyRequest = request.defaults({baseUrl: 'http://localhost:4567/api'});

app.get("/api/search", function (req , res) {
  console.log('===> got a query:', req.query.q );
  if (proxy){
    //proxyRequest({url: '/', qs: req.query})
    proxyRequest({url: '/search', qs: req.query})
      // .on('response', response => {
      // })
      .on('error',(err) => {
        console.log('error proxying.', err);
        res.sendFile(__dirname + '/_sample_search_test_pn0.json');
      })
      .pipe(res);
  } else {
    console.log('   => sending static local response');
    setTimeout( () => res.sendFile(__dirname + '/_sample_search_test_pn0.json'), 200)
  }
} )

app.get("/api/xfers", function (req , res) {
  if (proxy){
    proxyRequest('/xfers')
      .on('error',(err) => {
        console.log('error proxying.', err);
        res.sendFile(__dirname + '/_sample_xfers_output.json');
      })
      .pipe(res);
  } else {
    console.log('   => sending static local response');
    setTimeout( () => res.sendFile(__dirname + '/_sample_xfers_output.json'), 200)
  }
} )

app.get('/*', function(req, res) {
  res.sendFile( config.output.path + req.url )
})


app.post("/api/xfers", function (postReq , res) {
  console.log('===> got a xfers POST request');
  console.dir(postReq.body);
  if (proxy) {
    proxyRequest({
      method: 'POST',
      uri: '/xfers',
      headers: {'Content-type': 'application/json'},
      body: postReq.body
    })
      .on('error', (err) => console.log('error proxying POST', err))
      .pipe(res);
  } else {
    res.status(201).send('yeah');
  }
} )

app.delete("/api/xfers/:xferNum", function (delReq , res) {
  var xferNum = delReq.params.xferNum;

  console.log('===> got a xfers DELETE request for xfer', xferNum);
  if (proxy) {
    proxyRequest({
      method: 'DELETE',
      uri: `/xfers/${xferNum}`,
    })
      .on('request', (req, res) => console.log({req}, {res}))
      .on('error', (err) => console.log('error proxying DELETE', err))
      .pipe(res);
  } else {
    res.send("ok", 200);
  }
} )

app.listen(port, function(error) {
  if (error) {
    console.error(error)
  } else {
    console.info("==> 🌎  Listening on port %s. Open up http://localhost:%s/ in your browser.", port, port)
  }
})
